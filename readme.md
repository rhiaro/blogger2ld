#Blogger2LD

Blogger RSS feed to Linked Data (static).

## Ontology terms

Listed as I use them..

### Dublin Core

`Creator`: of posts, obviously.

### SIOC

`Post`: subclass of `foaf:Document`.

`Comment`: subclass of `sioc:Post`.

`BlogPost`: subclass of `sioc:Post`.

`MicroblogPost`: subclass of `sioc:Post`.

`topic`: a subclass of `dc:subject`, but I think this is more specific to blogs and stuff. I'll use it for freeform tags, that Blogger calls 'labels'. 

`Category`: a class for a `sioc:topic` to indicate it is a fixed category. I might use this instead of List, which is too specific to me.

### Mine own for want of better

`InfoPost`: subclass of sioc:Post that's very specific to my needs at the moment. Probably needs rethinking. For short blog posts that are informative rather than narrative.